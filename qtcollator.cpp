/** This file is part of QtCollator **

Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*

Contact:  Nokia Corporation (info@qt.nokia.com)**

GNU Lesser General Public License Usage
This file may be used under the terms of the GNU Lesser General Public License
version 2.1 as published by the Free Software Foundation and appearing in the
file LICENSE.LGPL included in the packaging of this file. Please review the
following information to ensure the GNU Lesser General Public License version
2.1 requirements will be met:
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.

In addition, as a special exception, Nokia gives you certain additional rights.
These rights are described in the Nokia Qt LGPL Exception version 1.1, included
in the file LGPL_EXCEPTION.txt in this package.

GNU General Public License Usage
Alternatively, this file may be used under the terms of the GNU General Public
License version 3.0 as published by the Free Software Foundation and appearing
in the file LICENSE.GPL included in the packaging of this file. Please review
the following information to ensure the GNU General Public License version 3.0
requirements will be met: http://www.gnu.org/copyleft/gpl.html.

Other Usage
Alternatively, this file may be used in accordance with the terms and
conditions contained in a signed written agreement between you and Nokia.

*/

#include "qtcollator.h"

#include <unicode/utypes.h>
#include <unicode/ucol.h>
#include <unicode/ustring.h>

#include "qdebug.h"

QT_BEGIN_NAMESPACE

class QtCollatorPrivate
{
public:
    QBasicAtomicInt ref;
    bool modified;
    QLocale locale;
    QtCollator::Strength strength;
    QtCollator::Options options;

    UCollator *collator;

    QtCollatorPrivate()
        : modified(true),
          strength(QtCollator::TertiaryStrength),
          options(0),
          collator(0)
    { ref = 1; }

    int compare(ushort *s1, int len1, ushort *s2, int len2);
};

QtCollator::QtCollator(const QLocale &locale)
    : d(new QtCollatorPrivate)
{
    setLocale(locale);
}

QtCollator::QtCollator(const QtCollator &other)
    : d(other.d)
{
    d->ref.ref();
}

QtCollator::~QtCollator()
{
    if (!d->ref.deref()) {
        if (d->collator)
            ucol_close(d->collator);
    }
}

QtCollator &QtCollator::operator=(const QtCollator &other)
{
    if (this != &other) {
        if (!d->ref.deref()) {
            if (d->collator)
                ucol_close(d->collator);
        }
        *d = *other.d;
        d->ref.ref();
    }
    return *this;
}

void QtCollator::setLocale(const QLocale &locale)
{
    if (d->ref != 1)
        detach();
    if (d->collator)
        ucol_close(d->collator);
    d->locale = locale;

    UErrorCode status = U_ZERO_ERROR;
    QByteArray name = locale.bcp47Name().replace(QLatin1Char('-'), QLatin1Char('_')).toLatin1();
    d->collator = ucol_open(name.constData(), &status);
    if (U_FAILURE(status))
        qWarning("Could not create collator: %d", status);

    // enable normalization by default
    ucol_setAttribute(d->collator, UCOL_NORMALIZATION_MODE, UCOL_ON, &status);

    // fetch options from the collator
    d->options = 0;

    switch (ucol_getAttribute(d->collator, UCOL_CASE_FIRST, &status)) {
    case UCOL_UPPER_FIRST: d->options |= QtCollator::PreferUpperCase; break;
    case UCOL_LOWER_FIRST: d->options |= QtCollator::PreferLowerCase; break;
    case UCOL_OFF:
    default:
        break;
    }

    switch (ucol_getAttribute(d->collator, UCOL_FRENCH_COLLATION, &status)) {
    case UCOL_ON: d->options |= QtCollator::FrenchCollation; break;
    case UCOL_OFF:
    default:
        break;
    }

    switch (ucol_getAttribute(d->collator, UCOL_ALTERNATE_HANDLING, &status)) {
    case UCOL_SHIFTED: d->options |= QtCollator::IgnorePunctuation; break;
    case UCOL_NON_IGNORABLE:
    default:
        break;
    }


    switch (ucol_getAttribute(d->collator, UCOL_CASE_LEVEL, &status)) {
    case UCOL_ON: d->options |= QtCollator::ExtraCaseLevel; break;
    case UCOL_OFF:
    default:
        break;
    }

    switch (ucol_getAttribute(d->collator, UCOL_HIRAGANA_QUATERNARY_MODE, &status)) {
    case UCOL_ON: d->options |= QtCollator::HiraganaQuaternaryMode; break;
    case UCOL_OFF:
    default:
        break;
    }

    switch (ucol_getAttribute(d->collator, UCOL_NUMERIC_COLLATION, &status)) {
    case UCOL_ON: d->options |= QtCollator::NumericMode; break;
    case UCOL_OFF:
    default:
        break;
    }
}

QLocale QtCollator::locale() const
{
    return d->locale;
}

void QtCollator::detach()
{
    if (d->ref != 1) {
        QtCollatorPrivate *x = new QtCollatorPrivate;
        x->ref = 1;
        x->strength = d->strength;
        x->options = d->options;
        x->modified = true;
        x->collator = 0;
        if (!d->ref.deref()) {
            if (d->collator)
                ucol_close(d->collator);
        }
        d = x;
    }
}

void QtCollator::setStrength(QtCollator::Strength strength)
{
    if (d->ref != 1)
        detach();

    switch (strength) {
    case QtCollator::PrimaryStrength:    ucol_setStrength(d->collator, UCOL_PRIMARY); break;
    case QtCollator::SecondaryStrength:  ucol_setStrength(d->collator, UCOL_SECONDARY); break;
    case QtCollator::TertiaryStrength:   ucol_setStrength(d->collator, UCOL_TERTIARY); break;
    case QtCollator::QuaternaryStrength: ucol_setStrength(d->collator, UCOL_QUATERNARY); break;
    case QtCollator::IdenticalStrength:  ucol_setStrength(d->collator, UCOL_IDENTICAL); break;
    default:
        qWarning("Invalid strength mode %d", strength);
    }
}

QtCollator::Strength QtCollator::strength() const
{
    switch (ucol_getStrength(d->collator)) {
    case UCOL_PRIMARY:   return QtCollator::PrimaryStrength;
    case UCOL_SECONDARY: return QtCollator::SecondaryStrength;
    case UCOL_QUATERNARY:return QtCollator::QuaternaryStrength;
    case UCOL_IDENTICAL: return QtCollator::IdenticalStrength;
    case UCOL_TERTIARY:
    default:
        return QtCollator::TertiaryStrength;
    }
    return QtCollator::TertiaryStrength;
}

void QtCollator::setOptions(QtCollator::Options options)
{
    if (d->ref != 1)
        detach();

    d->options = options;

    UErrorCode status = U_ZERO_ERROR;

    if (options & QtCollator::PreferUpperCase)
        ucol_setAttribute(d->collator, UCOL_CASE_FIRST, UCOL_UPPER_FIRST, &status);
    else if (options & QtCollator::PreferLowerCase)
        ucol_setAttribute(d->collator, UCOL_CASE_FIRST, UCOL_LOWER_FIRST, &status);
    else
        ucol_setAttribute(d->collator, UCOL_CASE_FIRST, UCOL_OFF, &status);
    if (U_FAILURE(status))
        qWarning("ucol_setAttribute: Case First failed: %d", status);

    ucol_setAttribute(d->collator, UCOL_FRENCH_COLLATION,
                      options & QtCollator::FrenchCollation ? UCOL_ON : UCOL_OFF, &status);
    if (U_FAILURE(status))
        qWarning("ucol_setAttribute: French collation failed: %d", status);

    ucol_setAttribute(d->collator, UCOL_NORMALIZATION_MODE,
                      options & QtCollator::DisableNormalization ? UCOL_OFF : UCOL_ON, &status);
    if (U_FAILURE(status))
        qWarning("ucol_setAttribute: Normalization mode failed: %d", status);

    ucol_setAttribute(d->collator, UCOL_ALTERNATE_HANDLING,
                      (options & QtCollator::IgnorePunctuation) ? UCOL_SHIFTED : UCOL_NON_IGNORABLE, &status);
    if (U_FAILURE(status))
        qWarning("ucol_setAttribute: Alternate handling failed: %d", status);

    ucol_setAttribute(d->collator, UCOL_CASE_LEVEL,
                      options & QtCollator::ExtraCaseLevel ? UCOL_ON : UCOL_OFF, &status);
    if (U_FAILURE(status))
        qWarning("ucol_setAttribute: Case level failed: %d", status);

    ucol_setAttribute(d->collator, UCOL_HIRAGANA_QUATERNARY_MODE,
                      options & QtCollator::HiraganaQuaternaryMode ? UCOL_ON : UCOL_OFF, &status);
    if (U_FAILURE(status))
        qWarning("ucol_setAttribute: hiragana mode failed: %d", status);

    ucol_setAttribute(d->collator, UCOL_NUMERIC_COLLATION,
                      options & QtCollator::NumericMode ? UCOL_ON : UCOL_OFF, &status);
    if (U_FAILURE(status))
        qWarning("ucol_setAttribute: numeric collation failed: %d", status);
}

QtCollator::Options QtCollator::options() const
{
    return d->options;
}

int QtCollator::compare(const QString &s1, const QString &s2) const
{
    return d->compare((ushort *)s1.constData(), s1.size(), (ushort *)s2.constData(), s2.size());
}

int QtCollator::compare(const QStringRef &s1, const QStringRef &s2) const
{
    return d->compare((ushort *)s1.constData(), s1.size(), (ushort *)s2.constData(), s2.size());
}

int QtCollatorPrivate::compare(ushort *s1, int len1, ushort *s2, int len2)
{
    const UCollationResult result =
            ucol_strcoll(collator, (const UChar *)s1, len1, (const UChar *)s2, len2);
    return result;
}

QByteArray QtCollator::sortKey(const QString &string) const
{
    QByteArray result(16 + string.size() + (string.size() >> 2), Qt::Uninitialized);
    int size = ucol_getSortKey(d->collator, (const UChar *)string.constData(),
                               string.size(), (uint8_t *)result.data(), result.size());
    if (size > result.size()) {
#ifdef _DEBUG
        qDebug() << "### sortKey realloc from" << result.size() << "to" << size << "for string" << string.size();
#endif
        result.resize(size);
        size = ucol_getSortKey(d->collator, (const UChar *)string.constData(),
                               string.size(), (uint8_t *)result.data(), result.size());
    }
    result.truncate(size);
    return result;
}
QT_END_NAMESPACE
