/** This file is part of QtCollator **

Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*

Contact:  Nokia Corporation (info@qt.nokia.com)**

GNU Lesser General Public License Usage
This file may be used under the terms of the GNU Lesser General Public License
version 2.1 as published by the Free Software Foundation and appearing in the
file LICENSE.LGPL included in the packaging of this file. Please review the
following information to ensure the GNU Lesser General Public License version
2.1 requirements will be met:
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.

In addition, as a special exception, Nokia gives you certain additional rights.
These rights are described in the Nokia Qt LGPL Exception version 1.1, included
in the file LGPL_EXCEPTION.txt in this package.

GNU General Public License Usage
Alternatively, this file may be used under the terms of the GNU General Public
License version 3.0 as published by the Free Software Foundation and appearing
in the file LICENSE.GPL included in the packaging of this file. Please review
the following information to ensure the GNU General Public License version 3.0
requirements will be met: http://www.gnu.org/copyleft/gpl.html.

Other Usage
Alternatively, this file may be used in accordance with the terms and
conditions contained in a signed written agreement between you and Nokia.

*/

#include <QtCore>
#include <QtTest>

#include "qtcollator.h"

class tst_QtCollator: public QObject
{
    Q_OBJECT

private:
    QStringList chileanData, chileanWords;
    QStringList castellanoData, castellanoWords;

    QStringList allData, allWords;

    QStringList read(const QString &filename);
    QStringList splitWords(const QStringList &list);

public slots:
    void initTestCase();
    void cleanupTestCase();

private slots:
    void localeAwareCompare_data();
    void localeAwareCompare();

    void normal_data();
    void normal();
};

QStringList tst_QtCollator::splitWords(const QStringList &list)
{
    QStringList result;
    foreach (const QString &s, list)
        result << s.split(" ");
    return result;
}

QStringList tst_QtCollator::read(const QString &filename)
{
    QFile file(filename);
    if (!file.open(QFile::ReadOnly))
        return QStringList();

    QByteArray data = file.readAll();
    return QString::fromUtf8(data.constData(), data.size()).split("\n");
}

void tst_QtCollator::initTestCase()
{
    chileanData = read("chilean.txt");
    chileanWords = splitWords(chileanData);
    castellanoData = read("castellano.txt");
    castellanoWords = splitWords(castellanoData);

    allData << chileanData << castellanoData;
    allWords << chileanWords << castellanoWords;
}

void tst_QtCollator::cleanupTestCase()
{

}

void tst_QtCollator::localeAwareCompare_data()
{
    normal_data();
}

void tst_QtCollator::normal_data()
{
    QTest::addColumn<QString>("locale");
    QTest::addColumn<QStringList>("data");

    QTest::newRow("pt-PT:chilean:data") << "pt-PT" << chileanData;
    QTest::newRow("pt-PT:chilean:words") << "pt-PT" << chileanWords;
    QTest::newRow("pt-PT:castellano:data") << "pt-PT" << castellanoData;
    QTest::newRow("pt-PT:castellano:words") << "pt-PT" << castellanoWords;

    QTest::newRow("pt-PT:all:data") << "pt-PT" << allData;
    QTest::newRow("pt-PT:all:words") << "pt-PT" << allWords;

    QTest::newRow("zh-CN:all:data") << "zh-CN" << allData;
    QTest::newRow("zh-CN:all:words") << "zh-CN" << allWords;

    QTest::newRow("fr-FR:all:data") << "fr-FR" << allData;
    QTest::newRow("fr-FR:all:words") << "fr-FR" << allWords;

    QTest::newRow("fr-CA:all:data") << "fr-CA" << allData;
    QTest::newRow("fr-Ca:all:words") << "fr-CA" << allWords;
}

void tst_QtCollator::normal()
{
    QFETCH(QString, locale);
    QFETCH(QStringList, data);

    QtCollator collator(locale);

    QBENCHMARK {
        qSort(data.begin(), data.end(), collator);
    }
}

struct Comparator
{
    inline bool operator()(const QString &s1, const QString &s2) const
    { return s1.localeAwareCompare(s2) < 0; }
};

void tst_QtCollator::localeAwareCompare()
{
    QFETCH(QString, locale);
    QFETCH(QStringList, data);

   Comparator c;

    QBENCHMARK {
        qSort(data.begin(), data.end(), c);
    }
}


QTEST_MAIN(tst_QtCollator)

#include "main.moc"
