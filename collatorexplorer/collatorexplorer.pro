TEMPLATE = app
TARGET = 
DEPENDPATH += .

INCLUDEPATH += . ..
win32:INCLUDEPATH += . .. c:/users/denis/dev/icu/include

unix:LIBS += -licuuc -licui18n
win32:LIBS += -Lc:/users/denis/dev/icu/lib -licuin

# Input
HEADERS += ../qtcollator.h
SOURCES += main.cpp ../qtcollator.cpp
