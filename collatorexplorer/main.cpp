/** This file is part of QtCollator **

Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*

Contact:  Nokia Corporation (info@qt.nokia.com)**

GNU Lesser General Public License Usage
This file may be used under the terms of the GNU Lesser General Public License
version 2.1 as published by the Free Software Foundation and appearing in the
file LICENSE.LGPL included in the packaging of this file. Please review the
following information to ensure the GNU Lesser General Public License version
2.1 requirements will be met:
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.

In addition, as a special exception, Nokia gives you certain additional rights.
These rights are described in the Nokia Qt LGPL Exception version 1.1, included
in the file LGPL_EXCEPTION.txt in this package.

GNU General Public License Usage
Alternatively, this file may be used under the terms of the GNU General Public
License version 3.0 as published by the Free Software Foundation and appearing
in the file LICENSE.GPL included in the packaging of this file. Please review
the following information to ensure the GNU General Public License version 3.0
requirements will be met: http://www.gnu.org/copyleft/gpl.html.

Other Usage
Alternatively, this file may be used in accordance with the terms and
conditions contained in a signed written agreement between you and Nokia.

*/

#include <QtGui>
#include "qtcollator.h"

class Widget : public QWidget
{
    Q_OBJECT
public:
    Widget();

public slots:
    void sort();
    void dataComboChanged(int);
    void localeComboChanged(int);

private:
    void fetchOptions();
    bool loadFile(const QString &name);
    void loadDataForLocale(const QString &name);

    QComboBox *localeCombo;
    QComboBox *dataCombo;
    QTextEdit *textEdit1;
    QTextEdit *textEdit2;
    QComboBox *strengthCombo;
    QComboBox *caseCombo;
    QCheckBox *frenchCheckBox;
    QCheckBox *normalizationCheckBox;
    QCheckBox *ignorePunctuationCheckBox;
    QCheckBox *caseLevelCheckBox;
    QCheckBox *hiraganaCheckBox;
    QCheckBox *numericCheckBox;

    QtCollator collator;
};

Widget::Widget()
{
    QLocale system = QLocale::system();
    localeCombo = new QComboBox;
    QList<QLocale> locales = QLocale::matchingLocales(QLocale::AnyLanguage, QLocale::AnyScript, QLocale::AnyCountry);
    localeCombo->addItem(QLatin1String("System: ") + system.bcp47Name(), system);
    foreach (QLocale l, locales)
        localeCombo->addItem(l.bcp47Name(), l);
    localeCombo->setCurrentIndex(0);
    connect(localeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(localeComboChanged(int)));

    QHBoxLayout *l = new QHBoxLayout(this);
    textEdit1 = new QTextEdit;
    QWidget *panel = new QWidget;
    textEdit2 = new QTextEdit;
    QVBoxLayout *lv = new QVBoxLayout;
    dataCombo = new QComboBox;
    lv->addWidget(dataCombo);
    lv->addWidget(textEdit1);
    l->addLayout(lv);
    l->addWidget(panel);
    l->addWidget(textEdit2);

    QVBoxLayout *v = new QVBoxLayout(panel);

    v->addWidget(localeCombo);

    QPushButton *sortButton = new QPushButton("Sort");
    connect(sortButton, SIGNAL(clicked()), this, SLOT(sort()));
    v->addWidget(sortButton);

    strengthCombo = new QComboBox;
    strengthCombo->addItem("Primary (Base Letters)", (int)QtCollator::PrimaryStrength);
    strengthCombo->addItem("Secondary (Primary + Accents)", (int)QtCollator::SecondaryStrength);
    strengthCombo->addItem("Tertiary (Secondary + Case )", (int)QtCollator::TertiaryStrength);
    strengthCombo->addItem("Quaternary (Tertiary + Punct.)", (int)QtCollator::QuaternaryStrength);
    strengthCombo->addItem("Identical (Quaternary + Codepoint)", (int)QtCollator::IdenticalStrength);
    strengthCombo->setCurrentIndex(2);
    v->addWidget(strengthCombo);

    caseCombo = new QComboBox;
    caseCombo->addItem("Don't force case", (int)0);
    caseCombo->addItem("Force Lowercase first", (int)QtCollator::PreferLowerCase);
    caseCombo->addItem("Force Uppercase first", (int)QtCollator::PreferUpperCase);
    caseCombo->setCurrentIndex(0);
    v->addWidget(caseCombo);

    frenchCheckBox = new QCheckBox("French accents");
    v->addWidget(frenchCheckBox);
    normalizationCheckBox = new QCheckBox("String normalization");
    normalizationCheckBox->setChecked(true);
    v->addWidget(normalizationCheckBox);
    ignorePunctuationCheckBox = new QCheckBox("Ignore punctuation");
    v->addWidget(ignorePunctuationCheckBox);
    caseLevelCheckBox = new QCheckBox("Add case level");
    v->addWidget(caseLevelCheckBox);
    hiraganaCheckBox = new QCheckBox("Add Hiragana level");
    v->addWidget(hiraganaCheckBox);
    numericCheckBox = new QCheckBox("Numeric collation");
    v->addWidget(numericCheckBox);

    loadDataForLocale(system.bcp47Name().replace('-', '_'));
    localeComboChanged(0);
}

void Widget::loadDataForLocale(const QString &name)
{
    int index = -1;

    // get a list of data files
    QDir dir = QDir::current();
    // those data files contain texts from different places, typed manually or
    // copied a few words from wikipedia articles, etc.
    QStringList files = dir.entryList(QStringList("data_*.txt"));
    foreach (const QString &fileName, files) {
        QString localeName = fileName.left(fileName.size()-4).mid(5);
        dataCombo->addItem(QString("data.txt: %1").arg(localeName), QLocale(localeName));
        if (localeName == name)
            index = dataCombo->count()-1;
    }
    connect(dataCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(dataComboChanged(int)));
    dataCombo->setCurrentIndex(index);
}

void Widget::localeComboChanged(int idx)
{
    QLocale locale = localeCombo->itemData(idx).toLocale();
    collator.setLocale(locale);
    fetchOptions();
}

void Widget::dataComboChanged(int idx)
{
    QLocale locale = dataCombo->itemData(idx).toLocale();
    QString name = locale.bcp47Name().replace('-', '_');
    while (!name.isEmpty() && !loadFile(QLatin1String("data_")+name+".txt")) {
        name.truncate(name.lastIndexOf('_'));
    }
    if (name.isEmpty())
        loadFile(QLatin1String("data.txt"));
}

bool Widget::loadFile(const QString &name)
{
    QFile file(name);
    if (!file.open(QFile::ReadOnly))
        return false;

    QByteArray data = file.readAll();
    QString stringData = QString::fromUtf8(data.constData(), data.size());
    stringData = stringData.trimmed();
    textEdit1->setText(stringData);
    return true;
}

void Widget::sort()
{
    int value = strengthCombo->itemData(strengthCombo->currentIndex()).toInt();
    collator.setStrength(QtCollator::Strength(value));

    value = 0;

    value |= caseCombo->itemData(caseCombo->currentIndex()).toInt();
    if (!normalizationCheckBox->isChecked())
        value |= QtCollator::DisableNormalization;
    if (ignorePunctuationCheckBox->isChecked())
        value |= QtCollator::IgnorePunctuation;
    if (frenchCheckBox->isChecked())
        value |= QtCollator::FrenchCollation;
    if (caseLevelCheckBox->isChecked())
        value |= QtCollator::ExtraCaseLevel;
    if (hiraganaCheckBox->isChecked())
        value |= QtCollator::HiraganaQuaternaryMode;
    if (numericCheckBox->isChecked())
        value |= QtCollator::NumericMode;

    collator.setOptions(QtCollator::Options(value));

    QStringList stringList = textEdit1->toPlainText().split("\n");
    qSort(stringList.begin(), stringList.end(), collator);
    textEdit2->setText(stringList.join("\n"));

    foreach (const QString &s, stringList)
        collator.sortKey(s);
}

void Widget::fetchOptions()
{
    strengthCombo->setCurrentIndex((int)collator.strength()-1);

    if (collator.casePreference() == QtCollator::IgnoreCase)
        caseCombo->setCurrentIndex(0);
    else if (collator.casePreference() == QtCollator::LowerCase)
        caseCombo->setCurrentIndex(1);
    else if (collator.casePreference() == QtCollator::UpperCase)
        caseCombo->setCurrentIndex(2);

    frenchCheckBox->setChecked(collator.options() & QtCollator::FrenchCollation);
    normalizationCheckBox->setChecked(!(collator.options() & QtCollator::DisableNormalization));
    ignorePunctuationCheckBox->setChecked(collator.options() & QtCollator::IgnorePunctuation);
    caseLevelCheckBox->setChecked(collator.options() & QtCollator::ExtraCaseLevel);
    hiraganaCheckBox->setChecked(collator.options() & QtCollator::HiraganaQuaternaryMode);
    numericCheckBox->setChecked(collator.options() & QtCollator::NumericMode);
}


int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    Widget w;
    w.show();
    return app.exec();
}

#include "main.moc"
