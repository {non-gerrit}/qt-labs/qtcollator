/** This file is part of QtCollator **

Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*

Contact:  Nokia Corporation (info@qt.nokia.com)**

GNU Lesser General Public License Usage
This file may be used under the terms of the GNU Lesser General Public License
version 2.1 as published by the Free Software Foundation and appearing in the
file LICENSE.LGPL included in the packaging of this file. Please review the
following information to ensure the GNU Lesser General Public License version
2.1 requirements will be met:
http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.

In addition, as a special exception, Nokia gives you certain additional rights.
These rights are described in the Nokia Qt LGPL Exception version 1.1, included
in the file LGPL_EXCEPTION.txt in this package.

GNU General Public License Usage
Alternatively, this file may be used under the terms of the GNU General Public
License version 3.0 as published by the Free Software Foundation and appearing
in the file LICENSE.GPL included in the packaging of this file. Please review
the following information to ensure the GNU General Public License version 3.0
requirements will be met: http://www.gnu.org/copyleft/gpl.html.

Other Usage
Alternatively, this file may be used in accordance with the terms and
conditions contained in a signed written agreement between you and Nokia.

*/

#include <QtCore>
#include "qtcollator.h"

void help()
{
    qApp->quit();
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QString localeName;
    enum Option { Default, On, Off };
    Option numericMode = Default;
    Option lowerCase = Default;
    Option upperCase = Default;
    Option french = Default;
    Option ignorePunctuation = Default;

    QStringList args = app.arguments();
    for (int i = 1; i < args.size();) {
        QString arg = args.at(i);
        bool value = true;
        if (arg.startsWith(QLatin1String("-no-"))) {
            value = false;
            arg = arg.mid(3);
        }
        if (arg == "-locale") {
            ++i;
            if (args.size() >= i+1)
                help();
            localeName = args.at(i);
        } else if (arg == "-numeric") {
            numericMode = value ? On : Off;
        } else if (arg == "-uppercase") {
            upperCase = value ? On : Off;
        } else if (arg == "-lowercase") {
            lowerCase = value ? On : Off;
        } else if (arg == "-french") {
            french = value ? On : Off;
        } else if (arg == "-ignorepunctuation") {
            ignorePunctuation = value ? On : Off;
        }
        ++i;
    }

    QTextStream input(stdin, QIODevice::ReadOnly);
    QStringList data;
    while (!input.atEnd())
        data.append(input.readLine());
    if (data.last().trimmed().isEmpty())
        data.pop_back();

    QtCollator collator(localeName.isEmpty() ? QLocale() : QLocale(localeName));
    if (numericMode != Default)
        collator.setNumericMode(numericMode == On);
    if (lowerCase != Default)
        collator.setCasePreference(lowerCase == On ? QtCollator::LowerCase : QtCollator::IgnoreCase);
    if (upperCase != Default)
        collator.setCasePreference(upperCase == On ? QtCollator::UpperCase : QtCollator::IgnoreCase);
    if (french != Default)
        collator.setOptions(collator.options() & QtCollator::FrenchCollation);
    if (ignorePunctuation != Default)
        collator.setOptions(collator.options() & QtCollator::IgnorePunctuation);

    qSort(data.begin(), data.end(), collator);

    QTextStream output(stdout, QIODevice::WriteOnly);
    foreach (const QString &s, data)
        output << s << QLatin1String("\n");

    return 0;
}
